package agh.bo.web.webdriver.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Bartosz Janota
 * Date: 12/17/13
 * Time: 8:37 AM
 */
public class PoshWolfPage {

    @FindBy(xpath = ".//*[@id='20-10']")
    public WebElement twentyJobsTenMachines;

    @FindBy(xpath = ".//*[@id='3']")
    public WebElement testThree;

    @FindBy(xpath = ".//*[@id='launch-sample-btn']")
    public WebElement sampleButton;

    @FindBy(xpath = ".//*[@name='Visualize']")
    public WebElement taskAnimButton;

    @FindBy(css = "#machine-10 #text")
    public WebElement doneTenLabel;

    @FindBy(xpath = ".//*[@id='back-button']")
    public WebElement backButton;

    @FindBy(xpath = ".//*[@id='replay-btn']")
    public WebElement replayButton;



    public PoshWolfPage(WebDriver driver){
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Config.AJAX_TIMEOUT),this);
    }
}
