package agh.bo.web.webdriver.task0;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import agh.bo.web.webdriver.pages.PoshWolfPage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: Bartosz Janota
 * Date: 12/17/13
 * Time: 9:05 AM
 */
public class Task0Test {
    private static final String DEF_URL = "http://posh-wolf.herokuapp.com";
    private static WebDriver driver;
    private PoshWolfPage poshWolfPage;

    @BeforeClass
    public static void init(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Before
    public void setUP(){
        driver.get(DEF_URL);
        poshWolfPage = new PoshWolfPage(driver);
    }

    @org.junit.Test
    public void task0() throws Exception {
        int timeLag = 2;

        driver.manage().timeouts().implicitlyWait(timeLag, TimeUnit.SECONDS);
        assertEquals("20 jobs, 10 machines", poshWolfPage.twentyJobsTenMachines.getText());
        poshWolfPage.twentyJobsTenMachines.click();
        driver.manage().timeouts().implicitlyWait(timeLag, TimeUnit.SECONDS);

        assertEquals("Test #3",poshWolfPage.testThree.getText());
        poshWolfPage.testThree.click();
        driver.manage().timeouts().implicitlyWait(timeLag, TimeUnit.SECONDS);

        assertEquals("Launch", poshWolfPage.sampleButton.getText());
        poshWolfPage.sampleButton.click();
        driver.manage().timeouts().implicitlyWait(timeLag, TimeUnit.SECONDS);

        WebDriverWait waitVisualize = new WebDriverWait(driver, 10);
        WebElement elementVisualize = waitVisualize.until(
                ExpectedConditions.elementToBeClickable(poshWolfPage.taskAnimButton));

        assertEquals("Visualize", poshWolfPage.taskAnimButton.getText());
        poshWolfPage.taskAnimButton.click();
        driver.manage().timeouts().implicitlyWait(timeLag, TimeUnit.SECONDS);

        WebDriverWait waitReplay = new WebDriverWait(driver, 100);
        WebElement elementReplay = waitReplay.until(
                ExpectedConditions.elementToBeClickable(poshWolfPage.replayButton));

        assertEquals("Done", poshWolfPage.doneTenLabel.getText());
        driver.manage().timeouts().implicitlyWait(timeLag, TimeUnit.SECONDS);
        poshWolfPage.backButton.click();

        driver.manage().timeouts().implicitlyWait(timeLag, TimeUnit.SECONDS);

    }

    @After
    public void tearDown() throws InterruptedException {
        Thread.sleep(5000);
    }

    @AfterClass
    public static void clean() {
        driver.quit();
    }

}
